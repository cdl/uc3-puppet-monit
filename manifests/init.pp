class uc3_monit {
  $base_dir    = "/$id"
  $log_dir     = "${base_dir}/logs"
  $monit_bin   = "${base_dir}/local/bin/monit"
  $monit_dir   = "${base_dir}/apps/monit"
  $monitrc     = "${monit_dir}/monitrc"
  $monitrc_dir = "${monit_dir}/monitrc.d"
  $pid_file    = "${monit_dir}/monit.pid"

  file {
    $monit_dir :
      ensure  => directory;
    $monitrc_dir :
      ensure  => directory;
    "${base_dir}/.monitrc" :
      ensure  => symlink,
      target  => $monitrc;
    $monitrc :
      owner   => $id,
      group   => $id,
      mode    => "0700",
      content => template("uc3_monit/monitrc.erb");
  }

  tidy { $monitrc_dir :
    age     => "0",
    backup  => true,
    matches => "*",
    recurse => 1,
    notify  => Service["monit"];
  }
  
  uc3_init::script { "monit" :
    pid_file => $pid_file,
    start   => "${monit_bin}",
    stop    => "${monit_bin} quit"
  }

  service { "monit" :
    ensure    => "running",
    hasstatus => true,
    provider  => "init",
    path      => $uc3_init::dir,
    require   => Uc3_Init::Script[ "monit" ],
    subscribe => File[ $monitrc ];
  }
}

define uc3_monit::rc($content) {
  $monitrc_dir = "${uc3_monit::dir}/monitrc.d"
  file { "${uc3_monit::monitrc_dir}/${name}" :
    content => $content,
    notify  => Service[ "monit" ];
  }
}
